---
title: "Énergeie libre et force contre électromotrice"
date: 2016-04-09
---
Points clefs pour réduire les dépenses énergétiques dans une machine tournante électrique :

- Lorsque le champ magnétique d'une bobine s'effondre, il est possible de récupérer le courant en sortie de la bobine provenant de l'effondrement du champ magnétique et stocker l'énergeie dans un cendensateur pour la réutiliser et réalimenter une bobine peu après. Le fait qu'une self décale dans le temps le passage du courant permet cela.
- Trouver un agencement de la machine tournante qui permet d'éviter que le champ magnétique soit orthogonal aux fils. Utiliser une technique de switching qui coupe les bobines lorsqu'elles sont en situation contre productive.
- Relié au premier point : alimenter les bobines par des impulsions qui prennent fin rapidement et récupérer l'énergie de l'effondrement du champ magnétique
- Réduire la résistance du fil. En restant sur du cuivre, c'est difficile.

Références : Paul Babcock.

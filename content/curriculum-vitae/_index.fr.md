---
title: Curriculum Vitæ
date: 2020-12-30T16:16:44.953Z
layout: single
---

## Expérience ##

### Freelance (juillet 2019 - présent) ###

- **septembre 2020 - présent, [Codde](https://codde.fr) :** Application Rails utilisant [StimulusJS](https://stimulusjs.org) et [UiBibz](http://hummel.link/ui-bibz/) pour les widgets (basé sur Bootstrap). Refonte entière de l'application.

- **septembre 2020, [Lancey](https://www.lancey.fr) :** Courte mission pour les accompagner sur [Terraform](https://www.terraform.io) et [Kubernetes](https://kubernetes.io/) sur [Digital Ocean](https://www.digitalocean.com/).

- **juillet - août 2019, [RunAs](https://www.runas.io/) :** Démarrage du projet avec
  les premières fonctionnalités. Utilise Elixir et Phoenix. Visible sur
  [GitHub](https://github.com/runasio/sna/).

### SquareScale (sept 2016 - juillet 2019) ###

[SquareScale](https://squarescale.com), service proposant une plateforme *"cloud"* pour exécuter des containers Docker applicatifs.

- **été 2017 - printemps 2019 :** Refonte de l'infrastructure de SquareScale.
  Gestion de clusters Etcd, Consul, Nomad avec Terraform et mise à jour sans
  perte par remplacement de machines. Adaptation des services SquareScale pour
  cette nouvelle architecture.

- **printemps 2016 - été 2017 :** maintenance et amélorations de SquareScale.
  Ajout de fonctionnalités moins essentielles, création d'une QA automatique
  pour tester en permanence notre produit. Correction de nombreux bugs sur des
  cas limites, en particulier dans les providers terraform qui peuvent gérer mal
  des cas d'utilisation intensives d'API avec du rate-limiting.. Création et
  refonte partielle du service de facturation permettant d'effectuer des
  paiements via Stripe sur des projets à la demande et en fonction de l'usage
  réel.

- **sept 2016 - printemps 2017 :** création de SquareScale, Automatisation de
  création d'infrastructure avec Terraform piloté par un service en Go.
  Automatisation de déploiement de serveurs Etcd et Consul avec scale up/down.
  Gestion de clients Nomad.

    Technologies: Go, Ruby on Rails, React, Terraform, Nomad, Consul, Etcd, CoreOS


### Sogilis (juil 2010 - sept 2016) ###

[Sogilis](http://www.sogilis.com), société de service en informatique

- **2015 - 2016 : Missions pour [Enalean](https://www.enalean.com/),** Ajout de fonctionnalités dans la
  forge Libre [Tuleap](https://www.tuleap.org/). Technologies: PHP.

- **2015 : Missions pour [NetCeler](http://netceler.com/),** développement
  d'un système de plugins pour leur système d'analyse de réseau d'acheminement
  d'électricité publique.  Frontend et backend web pour une interface permettant
  de configurer ces plugins. Création de service Java permettant d'exécuter ces
  plugins.  Technologies: Microservices, Java, Angular.

- **mai 2015 : Mission pour [Phoxygen](https://twitter.com/phoxygen_),** adaption
  pour les langues RTL dans Firefox OS

- **2014 - 2015 : Missions pour
  [FP-Conseil](http://www.fpc-ingenierie.fr/),** développement d'un système
  d'analyse de trames réseau pour du monitorer et sécuriser des postes
  électriques.  Détection d'anomalies en rapport avec des trames suspectes ou en
  rapport a des événements imprévus qui se déroulent. Technologies:
  Wireshark/tshark, Suricata, Lua, Qt/C++.

- **2014 : Mission pour Genii,** développement d'un prototype de boîtier
  multimédia pour une maison intelligente, avec application mobile Android.
  Technologies: Android, Qt/C++, Système Linux embarqué.

- **2014 : Mission pour MGI Coutier,** installation d'un serveur Git avec
  Puppet

- **2013 - 2015 Missions pour 3DKreaForm,** développement d'un logiciel
  dentaire en Qt/C++ avec traitements 3D utilisant VTK permettant de faire
  coincider une forme surfacique (format STL) avec une forme volumique (format
  DICOM).
  Technologies: Qt, C++, VTK, CMake, STL, DICOM

- **fin 2011 - jan 2012 Création d'une formation Git:** Développement d'une
  formation Git sur 3 jours, avec cours et TP administrée plusieurs fois par an
  jusqu'à mon départ de Sogilis.

- **sept 2011 - printemps 2013 : Vacations IUT (hors Sogilis):** Animation de travaux dirigés
  et travaux pratiques pour l'IUT2 de Grenoble dans lequel j'ai fait mon
  apprentissage pour les cours d'algorithmie en Ada. 2 années scolaires
  consécutives.

- **été 2011 : Mission pour [Codde](https://codde.fr),** évolutions sur le
  produit [EIME](https://codde.fr/nos-logiciels/eime/presentation-eime),
  application Ruby on Rails d'analyse d'impact écologique. Technologies: Ruby on
  Rails, Cucumber.

- **printemps 2011 : Mission pour Thalès,** développement d'une
  application de test au sol d'équipement aéronautique civil embarqué en Ada
  suivant la norme DO-178C. Technologies: Ada, DO-178C.

- **2010 - 2014 : Diverses missions pour [AdaCore](https://www.adacore.com/):**
  Multiples missions de développement du système d'information
  d'[AdaCore](https://www.adacore.com/) en Ada principalement. Missions
  réalisées principalement a mi-temps en parallèle d'autres missions.

    - Développement de la nouvelle génération de l'interface de support avec les
      clients AdaCore (frontend web et backend en Ada)
    - Gestion du bug-tracker interne et des mailing lists internes en Ada
    - Plugins Wordpress et ExpressionEngine à la marge, en PHP


## Apprentissage et expérience personnelle ##

  - **2014:** Apprentissage de Go, implication dans le projet
    [IPFS](https://ipfs.io/). Création de divers serveurs web en Go, utilisant le
    web sémentique entre autre.


  - **2014:** Apprentissage de Node.JS dans le contexte de systèmes P2P avec une
    DHT Kadmelia.

  - **2014:** Apprentissage de Docker.

  - **2012-2019 Expérience personnelle:** Installation d'un serveur mail personnel.

      - Utilisation de Courrier, puis Dovecot et Exim.
      - Gestion automatisée de déploiement via un système d'automatisation écrit
        en Shell et avec [redo](https://github.com/apenwarr/redo/).

  - **2008-2015 Experience personnelle:** Travail sur le compilateur
    [Lisaac](https://fr.wikipedia.org/wiki/Lisaac) (qui cible le langage C).

      - Travail sur du code avec problématiques de bootstrap.
      - 2010: Ajout de fonctions d'instrumentation pour voir la couverture de code.
      - 2010-2015: Tentative de réécriture avec un backend LLVM.

### Études d'ingénieur (2010) ###

  - **sept 2007 - été 2010: Diplôme d'Ingénieur,** [ESISAR](http://esisar.grenoble-inp.fr/) à Valence (groupe Grenoble INP)

      - **2010:** Stage [Sogilis](http://www.sogilis.com), développement de
        [XReq](http://www.open-do.org/projects/xreq/) en Ada

      - **2009:** Stage [CMR](http://www.cmr-group.com/):
        Développement d'un logiciel de configuration en Qt/C++ multiplateforme
        avec des widgets spécifiques pour leur système de monitoring maritime.

  - **sept 2005 - été 2007: DUT Génie informatique,** à
    l'[IUT2](https://iut2.univ-grenoble-alpes.fr/) de Grenoble

      - Apprentissage personnel en C, C++, Lua (avec bindings C)

  - **2008 Experience personnelle:** Modificatication du driver de trackpad Xorg
    pour ajouter des fonctions. En C, avec Git (première expérience).

  - **été 2007: Baccalauréat scientifique,** option sciences de l'ingénieur

  - Apprentissage personnel des systèmes Linux et réseau (DNS bind9, DHCP, Mandrake, Debian)

## Compétences ##

- Bien maîtrisé et utilisé couramment :
    - Linux, réseau
    - AWS
    - Terraform
    - Etcd, Consul, Nomad
    - Docker
    - Go
    - Git
    - HTML 4.01, CSS 2.1

- Connu mais moins utilisé :
    - C
    - C++
    - Qt
    - Ada
    - JavaScript

<!-- TODO: sortir les compétences du moment avec une visu sympa

| Année      | 2019 | 2018 | 2017 | 2016 | 2015 | 2014 | 2013 | 2012 | 2011 | 2010 | 2009 | Avant |
| ---------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ----- |
| React      | x    | x    | xx   |      |      |      |      |      |      |      |      |       |
| JavaScript |      | x    | x    | x    | x    | xx   |      |      |      |      |      | x     |
| HTML/CSS   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xxx   |
| Ada        |      |      |      |      |      | x    | xx   | xx   | xx   | x    |      | x     |
| PHP        |      |      | x    | x    | x    |      |      |      |      | x    | xx   | xxx   |
| C/C++      |      |      |      |      | xx   | xx   | xx   |      |      |      | xx   | xx    |
| Qt         |      |      |      |      | x    | x    | xx   |      |      |      | xx   |       |
| Linux      | xxx  | xxx  | xxx  | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xxx   |
| Réseau     | xx   | xx   | xx   | xx   | xx   | xx   | xx   | x    | x    | x    | x    | xx    |
| Git        | xx   | xx   | xx   | xxx  | xxx  | xxx  | xxx  | xxx  | xxx  | xx   | xx   | x     |
| Go         | xxx  | xxx  | xxx  | xx   | xx   | x    |      |      |      |      |      |       |
| Docker     | xx   | xx   | xx   | xx   | xx   | xx   |      |      |      |      |      |       |
| Terraform  | xxx  | xxx  | xxx  | x    |      |      |      |      |      |      |      |       |
| Nomad      | xx   | xx   | xx   | x    |      |      |      |      |      |      |      |       |
| Consul     | xx   | xx   | xx   | x    |      |      |      |      |      |      |      |       |
| Etcd       | xx   | xx   | xx   | x    |      |      |      |      |      |      |      |       |
-->

## Loisirs ##

- jardinage et animaux
- lecture
- philosophie politique, spiritualité
- informatique et réseau *done it right*

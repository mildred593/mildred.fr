---
title: Curriculum Vitæ
date: 2020-12-30T16:16:44.995Z
layout: single
---

## Experience ##

### Freelance (july 2019 - now) ###

- **september 2020 - now, [Codde](https://codde.fr) :** Rails application using [StimulusJS](https://stimulusjs.org) for front-end and [UiBibz](http://hummel.link/ui-bibz/) for the UI (based on Bootstrap). Entire rework of the application to a new version.

- **september 2020, [Lancey](https://www.lancey.fr) :** Short mission to help with [Terraform](https://www.terraform.io) and [Kubernetes](https://kubernetes.io/) over [Digital Ocean](https://www.digitalocean.com/).

- **july - august 2019, [RunAs](https://www.runas.io/):** Starting the first features
  using Elixir and Phoenix. Available on
  [GitHub](https://github.com/runasio/sna/).

### SquareScale (sept 2016 - july 2019) ###

[SquareScale](https://squarescale.com), is a platform as a service that uses the *"cloud"* to execute Docker containers based services.

- **summer 2017 - spring 2019 :** Rework of the complete SquareScale
  infrastructure. Management of Etcd, Consul and Nomad clusters with Terraform
  and updates without downtime by machine replacement. Upgrade of SquareScale
  own services to this new architecture.

- **spring 2016 - summer 2017 :** maintenance and improvements on SquareScale.
  Creation of an automatic QA service to monitor the platform health. Addition
  of less important features. Fix of numerous bugs on edge cases, in particular
  on terraform providers that do not always handle well the rate limimting that
  occurs when there is a high number of API requests. Creation and partial
  refactoring of a billing service that is collecting AWS usage and perform
  payments through Stripe.

- **september 2016 - spring 2017 :** Creation of SquareScale, Automation of
  infrastructure creation using Terraform that is started automatically by a
  small Go service. Automation of the deployment and maintenance of Consul and
  Etcd clusters with scale up/down. Automatic management of Nomad clients.

    Technologies: Go, Ruby on Rails, React, Terraform, Nomad, Consul, Etcd, CoreOS


### Sogilis (july 2010 - september 2016) ###

[Sogilis](http://www.sogilis.com), computer engineering service company

- **2015 - 2016 : Mission for [Enalean](https://www.enalean.com/),** Addition of
  features in the [Tuleap](https://www.tuleap.org/) open source forge.
  Technologies: PHP.

- **2015 : Missions for [NetCeler](http://netceler.com/),** development of a
  plugin system for their software which handles and analyse public electrical
  networks software. Frontend and backend web with an interface that manages
  those plugins. Creation of Java services to execute those plugins
  Technologies: Microservices, Java, Angular.

- **may 2015 : Mission for [Phoxygen](https://twitter.com/phoxygen_),** Adapting
  Firefox OS application for RTL languages.

- **2014 - 2015 : Missions for
  [FP-Conseil](http://www.fpc-ingenierie.fr/),** development of an analysis
  system that capture network frames to monitor and secure electrical stations.
  Anomaly detection from suspect frames about events that are not supposed to
  occur.
  Technologies: Wireshark/tshark, Suricata, Lua, Qt/C++.

- **2014 : Mission for Genii,** devloppment of a multimedia box prototype for
  intelligent houses with an Android application.
  Technologies: Android, Qt/C++, Embedded Linux with busybox.

- **2014 : Mission for MGI Coutier,** installation of a Git server using Puppet.

- **2013 - 2015 Missions for 3DKreaForm,** development of a dental software
  using C++/Qt with 3D imagery and algorithms using VTK. The core algorithm was
  able to reconcile a 3D surface (STL format) with a volumic image taken from a
  scanner (DICOM format) to allow better positioning of dental implants.
  Technologies: Qt, C++, VTK, CMake, STL, DICOM

- **late 2011 - january 2012 Creation of a Git training:** Creation of a Git
  training on 3 days with practicals. The training was taught multiple times a
  year until I left Sogilis.

- **september 2011 - spring 2013 : Computer Science practicals animation
  (outside of Sogilis):** Animation of practicals and exercice session in
  computer science for the Grenoble IUT2. The language used and taucht was Ada.
  this happened during two consecutive scholar years.

- **été 2011 : Mission for [Codde](https://codde.fr),** improvements on the
  [EIME](https://codde.fr/nos-logiciels/eime/presentation-eime) product,
  Ruby on Rails application for ecological impact analysis. Technologies: Ruby on
  Rails, Cucumber.

- **spring 2011 : Mission for Thalès,** development of a ground test software
  for civil flight equipment following the DO-178C norm.
  Technologies: Ada, DO-178C.

- **2010 - 2014 : Various missions for [AdaCore](https://www.adacore.com/):**
  Various missions to improve their information system in Ada. The missions were
  mostly part-time.

    - Development of the new generation of the support interface with their
      customers with a web frontend and an Ada backend.
    - management of their bug-tracker and mailing list system.
    - Wordpress plugins and ExpressionEngine in PHP


## Degrees and personal experience ##

  - **2014:** Learning Go, Implication on [IPFS](https://ipfs.io/) and help with
    drafting their IPLD layer. Creation of various webservers uwing Go and
    exploiting the semantic web.


  - **2014:** Learning Node.JS with P2P systems and creating a kadmelia DHT.

  - **2014:** learning of Docker.

  - **2012-2019 Expérience personnelle:** management of a personal mail server.

      - Administration of Courrier, Dovecot and then Exim.
      - Automatic deployments using a custom system written in Shell with
        [redo](https://github.com/apenwarr/redo/).
      - First use of Linux namespaces with LXC before Docker existed.

  - **2008-2015 personal experience:** Work on the
    [Lisaac](https://fr.wikipedia.org/wiki/Lisaac) compiler that outputs C code.

      - Work on code with bootstrap issues.
      - 2010: Created a code coverage tool for that language
      - 2010-2015: Trials to recreate a compiler that would target LLVM instead
        of C.

### Engineering degree (2010) ###

  - **sept 2007 - summer 2010: Engineering degree,** [ESISAR](http://esisar.grenoble-inp.fr/) in Valence (Grenoble INP)

      - **2010:** Internship [Sogilis](http://www.sogilis.com), creation of
        [XReq](http://www.open-do.org/projects/xreq/) in Ada

      - **2009:** Internship [CMR](http://www.cmr-group.com/):
        Creation of a configuration software in Qt/C++, multiplatform, with
        custom widgets, for their maritime monitoring system.

  - **sept 2005 - summer 2007: Computer Science DUT degree,** at
    Grenoble [IUT2](https://iut2.univ-grenoble-alpes.fr/)

      - Personal training in C, C++, and Lua (including C bindings)

  - **2008 Personal experience:** Improvement on the Xorg trackpad driver
    to add few functions. In C with mercurial first, then Git (first
    experience).

  - **summer 2007: scientific baccalaureate,** with engineering science option

  - Personal training in Linux systemd and networks (DNS bind9, DHCP, mandrake,
    Debian)

## Skills ##

- Used frequently and good knowledge :
    - Linux, network
    - AWS
    - Terraform
    - Etcd, Consul, Nomad
    - Docker
    - Go
    - Git
    - HTML 4.01, CSS 2.1

- Known, less frequeltly used :
    - C
    - C++
    - Qt
    - Ada
    - JavaScript

<!-- TODO: sortir les compétences du moment avec une visu sympa

| Year       | 2019 | 2018 | 2017 | 2016 | 2015 | 2014 | 2013 | 2012 | 2011 | 2010 | 2009 | Avant |
| ---------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ----- |
| React      | x    | x    | xx   |      |      |      |      |      |      |      |      |       |
| JavaScript |      | x    | x    | x    | x    | xx   |      |      |      |      |      | x     |
| HTML/CSS   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xxx   |
| Ada        |      |      |      |      |      | x    | xx   | xx   | xx   | x    |      | x     |
| PHP        |      |      | x    | x    | x    |      |      |      |      | x    | xx   | xxx   |
| C/C++      |      |      |      |      | xx   | xx   | xx   |      |      |      | xx   | xx    |
| Qt         |      |      |      |      | x    | x    | xx   |      |      |      | xx   |       |
| Linux      | xxx  | xxx  | xxx  | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xx   | xxx   |
| Réseau     | xx   | xx   | xx   | xx   | xx   | xx   | xx   | x    | x    | x    | x    | xx    |
| Git        | xx   | xx   | xx   | xxx  | xxx  | xxx  | xxx  | xxx  | xxx  | xx   | xx   | x     |
| Go         | xxx  | xxx  | xxx  | xx   | xx   | x    |      |      |      |      |      |       |
| Docker     | xx   | xx   | xx   | xx   | xx   | xx   |      |      |      |      |      |       |
| Terraform  | xxx  | xxx  | xxx  | x    |      |      |      |      |      |      |      |       |
| Nomad      | xx   | xx   | xx   | x    |      |      |      |      |      |      |      |       |
| Consul     | xx   | xx   | xx   | x    |      |      |      |      |      |      |      |       |
| Etcd       | xx   | xx   | xx   | x    |      |      |      |      |      |      |      |       |
-->

## Personal interests ##

- gardening and animals
- reading
- philosophy, politics and religion
- computers and network *done it right*

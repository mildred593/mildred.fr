---
title: "Le livre de Nuréa – Les chroniques du Ǧírkù 0"
date: 2017-04-27
---
Dans ce livre, nous suivons le peuple Mušidim dont nous allons ici tenter de tracer la chronologie.

Depuis les premiers temps, les Mušidim sont confronts aux Kingalàm qui sont leur ennemi juré. Nous verrons plus loin leur filiation que Šuhia va découvrir. Pour leur échapper, les Mušidim, au cours de leur voyages spaciaux, créent des trous noir en détruisant des étoiles, sans se rendre compte de la gravité de cette action. Ces trois noir leur permettaient de créer des failles dans l'espace-temps leur permettant de rejoindre un autre lieu et un autre temps et d'échapper a leurs adversaires.

(p105) Il est arrivé une fois qu'ils échouent dans la nébuleuse d'Orion et provoqua une réaction en chaîne imprévisible d'effondrement. Afin d'investiguer le problème, la reine Pištéš et son roi Éa'am décidèrent de créer la mission Zid afin de découvrir les répercussions de ce phénomène, et de découvrir également la vérité sur les Kingalàm. Ils embarquèrent dans un vaisseau capable de voyager dans le temps. Ils laissairent le royaume aux révérendes Agarin de l'Ombre que Pištéš avait engendré par parthénogenèse.

(p109) Plus tard, Ea'am revient seul et ne trouve plus Pištéš. Il repart la chercher sans attendre. 40 ans après, Pištéš revient seule également, effondrée d'avoir manqué Éa'am, elle repart aussitôt le chercher. Suite a cela, les référendes Agatin de l'Ombre se révoltèrent et la guerre s'en suivit.

Plus tard, la mission Zianna, emportant a son bord une certaine Šuhia part explorer l'espace. Cette mission se retrouve dans le futur où elle est confrontée aux Kingalàm qui traquent ses membres qi s'entretuent. Šuhia survit et le fait soigner par les Kingalàm qui lui l'amnésie temporairement et la renvoie dans le passé avec un de leur vaisseaux. Les Kingalàm lui révèle qu'ils sont des descendants des mušidim qui ont muté à travers de leux voyages dans le passé, lorsqu'ils se sont approchés de trop près du fond cosmique qui les a irradiés. Ils ne peuvent plus respirer normalement, il se servent du níama et d'appareillages à la place. Ils lui révèlent aussi le futur de son peuple.

Šuhia se retrouve en fait 40000 ans après son départ à bord d'un vaisseau Kingalàm. Elle est questionnée mais ne se souvient de rien. Plus tard, sa mémoire lui revient peu à peu, mais elle est alors blanchie.

(p103) Šuhia va alors de son propre chef tenter de coriger l'avenir de son peuple. Elle va entammer le projet Numun et implanta un grand nombre d'espèces sur terre. Elle va également par parthénogenèse donner naissance a une nombreuse lignée de matriarches sombres, qui ont pour mission d'améliorer les conditions politiques des mušidim. Las du futur qu'elle a entrevu, elle repart a bord du même vaisseau Kingalàm dans le futur, mais plus tôt que son précédent voyage. Suite a cela, elle sera déclarée traître et la procréation de ses matriarches sera contrôllée.

Pendant ce temps, Barbélú est née matriarche et découvre un grand nombre de ces choses lorsqu'elle est missionner pour travailler sur les problèmes avec Orion (l'ombre de Ga'anzír) et les Kingalàm. Les matriarches coient avoir découvert en elle la réincarnation de Pištéš et souhaitent la garder pour revenir sur le trône. Au lieu de cela, elle suit les conseils de la régente qui souhaite garder son trône et part dans une mission organisée dans l'ombre de Ga'anzír nommée Pištéš en l'honneur de la reine passée (p145). Suite a une altercation avec les Kingalám, elle se retrouvera sur terre dans le futur, après la seconde arrivée de Šuhia mais avant son premier voyage dans le futur.

Après cela, la terre connaît une grande extinction, qui met sans doute fin a ce que nous appelons le Trias poue entamer l'ère du Jurassique. Cette extinction est causée par des phénomènes météorologiques et des vents solaires exceptionnels causés par des formes pensées, en concordance avec ce qu'avait prédit Barbélú avant son départ.

C'est suite a cette extinction que Šuhia termine la dernière étape de son voyage. Elle crée les Kingú verts et rouges afin de l'aider dans sa tâche, et elle peuple la terre de dinausaures.

Šuhia, sans doute pour tenter d'alerter Pištéš et Éa'am et utilise des miroirs pour refléter la lumière et les arrêter. Pištéš dans son voyage temporel voit cette lumière et sort du vasseau, seule. Éa'am se retrouve seul dans le vaisseau, encore endormi. Sans doute des formes pensées au départ du couple les a empêché de se coordiner suffisament pour qu'ils réagisent en même temps.

Šuhia va alors tuer Pištéš. Il y a un problème de cohérence ici car qui donc va revenir seule dans le passé a bord de la machine Zida pour ensuite repartir. Peut être est-ce en fait Šuhia qui va se faire passer pour Pištéš pour ensuite disparaître dans un autre temps.

Quoi qu'il en soit, c'est dans ce monde façonné par Šuhia et avec Éa'am seul dans la machine Zida que Barbélú fa terminer son voyate temporel. La mission Pištéš a été attaquée par des Kingalám et Barbélú en est la seule rescapée, et c'est sur terre qu'elle atterit.

Un autre point a éclaircir est que Šuhia dit avoir tué par deux fois Barbélú a son arrivée auparavant. Comment a-t-elle pu être tuée plusieurs fois ? Quoi qu'il en soit, la troisième fois, elle sera épargnée car ayant déjà donné naissance a Ía'aldabaut, fruit de son union avec le Kingalám.

C'est ensuite qu'en présence de Barbélú, Šuhia va tuer Éa'am afin de le délivrer. C'est peut être Šuhia, probablement descendante des révérendes Agarin de l'Ombre et possédant la même apparence qui prendra place a bord de Zida pour repartir dans le passé, se faire passer pour Pištéš et enfin s'enfuir vers un autre monde.

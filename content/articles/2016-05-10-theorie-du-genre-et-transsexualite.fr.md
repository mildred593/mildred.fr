---
title: "Théorie Du Genre Et Transsexualité"
date: 2016-05-10
draft: true
---

*[Mildred](http://mildred.fr), 1er Février 2014, 9 mai 2016*

Qu'est-ce que la transsexualité ? Quel rapport avec la théorie du genre ? Comment les deux vont-ils ou ne vont-ils pas ensemble ? Je vous propose une petite exploration, fruit de mon expérience personnelle.

Définitions
-----------

Comment démarrer une discussion fructueuse sans parler de définitions. Dans le cas présent, je pense que c'est un exercice encore plus important dans la mesure où le sens des mots est délicat et on peut facilement leur faire dire l'inverse de leur essence.

Je vais employer les termes sexe et genre que je différencie, car la transsexualité se base justement sur une distinction de deux identités sexuées distinctes.

**transsexualité :** État d'une personne (dite transsexuelle) qui ressent un écart entre son identité sexuée profonde (ce qu'on nomme le genre) et son identité sexuée physique (le sexe).

**sexe :** Identité sexuée d'une personne sur le plan anatomique (mâle ou femelle).

**genre :** Identité sexuée ressentie d'une personne, souvent en accort avec son sexe, sauf pour les personnes transsexuelles.

**théorie du genre :** Théorie selon laquelle le genre d'une personne n'est pas inné mais acquis. En d'autres termes, cette théorie nous dit que l'identité sexuée, autre que purement physique, est une construction de la société et qu'elle peut être déconstruite, comme la religion, par exemple, a été déconstruite.

<!-- **hétéronormé :** Adjectif indiquant le respect des normes ou valeurs traditionnellement associées aux genres masculin et féminin. Peut s'employer de manière péjorative dans le contexte de la théorie du genre pour qualifier des personnes ne s'étant pas "émancipées" des rôles traditionnels comme cette théorie le prône. -->

La théorie du genre
-------------------

Souvent le terme genre est utilisé uniquement pour désigner la théorie du genre. Or, dans cet article, je soutiens que le genre et ce qu'on appelle communément la "théorie du genre" ne sont pas du tout la même chose. En tant que personne transsexuelle, je peux identifier de manière unique ce qu'est le genre, employé dans le sens que je donne plus haut. J'ai plus de mal a comprendre ce qu'on emploie par "théorie du genre", je vais donc explorer cela en premier :

Qu'est-ce donc que la théorie du genre ? C'est un courant de pensée qui se rapproche du féminisme, et vise a une égalité complète entre le genre masculin et féminin. Et plus qu'une égalité, la théorie du genre va même jusqu'a identifier les deux. Le masculin et le féminin n'existent plus vraiment. Ce sont des habitudes acquises pendant l'enfance. Un homme n'est plus différent d'une femme, a part sur le plan anatomique. Il est donc juste de ne pas élever les enfants de manière sexuée, mais de leur laisser choisir leur préférences. Voudront-ils être des hommes ou des femmes, c'est a eux de choisir. Sur le plan de la sexualité, lé théorie du genre va également abolir toute différences. L'homosexualité n'est plus une sexualité particulière et différente, c'est juste une sexualité entre deux personnes qui se trouvent avoir une anatomie identique. Un détail mineur somme toute.

Pour résumer : selon la théorie du genre, l'identité sexuée d'une personne est un attribut acquis.

Dans ce cadre, la transsexualité est souvent utilisée pour montrer la différente entre le sexe et le genre et automatiquement faire passer, sans le prouver, l'idée que le genre est acquis. Or ce n'est pas vrai. Des expériences ont été faites **ref nécessaire** montrant qu'un garçon élevé comme une fille se retrouvera a l'age adulte a devoir faire le chemin inverse pour redevenir un homme.

Le genre inné
-------------

J'opposerais a la théorie du genre, un autre courant de pensée que j'ai envie d'appeler *le genre inné*. Et j'ose penser que les personnes transsexuelles sont la preuve vivante que, du moins peut elles, c'est l'approche a considérer.

Il s'agit de considérer que le genre d'une personne, son identité sexuée, n'est pas sujet a changement. C'est quelque chose d'inné, fondamental a la constitution de la personne. Il existe des différences fondamentales entre une femme et un homme.

Dans ce cadre, la personne transsexuelle va être le cas particulier mettant en évidence cette propriété fondamentale. <!-- Oserais-je même penser que si les personnes transsexuelles sont associées au monde LGBT (Lesbiennes, Gay, Bi, Trans) qui prône la théorie du genre, c'est pour occulter cette vérité fondamentale. --> La personne transsexuelle est née avec des attributs sexuels différents de son identité sexuée. Son parcours va consister a rapprocher son identité, qui lui est fondamentale, avec son anatomie, en modifiant cette dernière le plus souvent.

La transsexualité et la théorie du genre
----------------------------------------

La transsexualité n'existe que parce que deux genres différents existent. Une personne transsexuelle va chercher naturellement a correspondre aux rôles traditionnels donnés par la société. C'est un impératif pour elle. Et c'est là qu'il y a un paradoxe. Alors que la transsexualité est souvent utilisé comme argument pour la théorie du genre, si la société parvenait a effacer la différence entre homme et femme, les personnes transsexuelles seront bien en peine a pour retrouver leur identité.

C'est là que je pense que la transsexualité n'a pas sa place au sein du monde LGBT. LGBT signifie Lesbienne, Gay, Bi, Trans, et c'est un mouvement dominé majoritairement par les personnes homosexuelles. Il soutient la théorie du genre parce qu'elle permet d'effacer la sexuation des individus et favorise de ce fait l'homosexualité, et la transition des personnes transsexuelles.

En apparence, cela facilite la transition pour les personnes transsexuelles, mais leur ôte également leur identité. Il arrive souvent même que des personnes transsexuelles se fassent reprocher leur vision hétéronormée du monde, et on les encourage a laisser tomber ces carcans pour accueillir une vision où la différence entre homme et femme n'existe plus. Mais, est-ce leur identité qu'on supprime ainsi ?




Recherches futures
------------------

- intersexualité : enfants élevés dans un genre différent de leur sexe anatomique, voir avec une modification génitale dés la naissance.
- transsexualité en Iran

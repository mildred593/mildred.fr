---
title: "Jeanne d'Arc"
date: 2020-09-17
---
Bonjour,

Je voulais vous entretenir de réalisations que j'eus à la lecture du procès de Jeanne d'Arc lu et commenté par Jacques Trémolet de Villiers, et à quel point son martyre est proche du nôtre actuel. En effet, chose que je n'avais pas réalisé jusqu'alors, Jeanne est confrontée au même dilemme auquel nous devons tous faire face actuellement.

En effet, Jeanne est jugée par des clercs qui cherchent à lui imposer une obéissance parfaite. Elle doit renier ses voix et la mission que Dieu lui à confiée car l'église, qui d'ailleurs avec trois papes n'était pas en bonne forme, à décidé que ses voix n'étaient pas de Dieu. Elle est forcément très tiraillée car elle viole en apparence l'article Unam Sanctam Ecclesiam en refusant l'autorité apparente qui la contraint pour garder sa foi. Sachant pertinemment que si elle violait réellement cet article, elle pêcherait gravement. Mais sachant également que se soumettre à l'autorité ecclésiale lui demandait de renier sa foi, ce qui serait également un péché grave.

Il n'empêche qu'elle est rendue victorieuse de cette épreuve et se sanctifie par elle en acceptant son martyre. Martyre qui se rapproche beaucoup de la passion de Notre Seigneur qui lui aussi dût être mis à mort comme le dernier des brigands.

L'histoire de Jeanne nous montre également qu'il faut se garder de pharisianisme, sans quoi nous serions obligé de condamner cette sainte pour de vulgaire motifs comme son habit d'homme et le fait que le métier des armes n'est pas normalement attribué aux femmes.

J'ai l'intuition que Jeanne nous montre la voie à suivre pour notre époque car nous aussi nous devons souffrir des mêmes épreuves. En particulier les accusations de schisme et d'insoumission sont extraordinairement actuelles. De même que Jeanne est tombée en signant de mauvaise grâce une abjuration en voulant repousser son martyre, de même, nous pouvons tomber et nous relever en acceptant notre martyre.

Je trouvais important de coucher cela par écrit. Prions Sainte Jeanne qu'elle nous guide dans notre épreuve.

---
title: "L'Église et Vatican II"
date: 2019-02-25
draft: false
---

Dans ma recherche sur la vérité de la foi, je me suis documentée sur un certain nombre de sujets concernant l’état de l’église, et j’aimerais discuter de quelques questions à ce sujet. Se présentent à moi trois thèses principales :

I. La première est celle des sédévacantistes, qui considèrent que le pape est infaillible, et qu'une conséquence de cette infaillibilité est qu'il serait impossible pour lui de professer des opinions modernistes. Ainsi, les papes depuis au moins Paul VI seraient des antipapes, car professant des opinions modernistes. Il semble que bien des sédévacantistes suppose une infaillibilité trop forte du pape. En effet, si on rappelle la définition qui est donnée par le concile Vatican I, on a le texte :

>   […] nous enseignons et proclamons comme un dogme révélé de Dieu :
>
>   Le pontife romain, lorsqu'il parle ex cathedra, c’est-à-dire lorsque, remplissant sa charge de pasteur et de docteur de tous les chrétiens, il définit, en vertu de sa suprême autorité apostolique, qu'une doctrine, en matière de foi ou de morale, doit être admise par toute l'Église, jouit par l'assistance divine à lui promise en la personne de saint Pierre, de cette infaillibilité dont le divin Rédempteur a voulu que fût pourvue l'Église, lorsqu'elle définit la doctrine sur la foi ou la morale. Par conséquent, ces définitions du Pontife romain sont irréformables de par elles-mêmes et non en vertu du consentement de l'Église.
>
>   Si quelqu'un, ce qu'à Dieu ne plaise, avait la présomption de contredire notre définition qu'il soit anathème

Pour reformuler, le pape doit (1) parler en tant que pape, et non docteur privé, (2) en s'adressant à l'église toute entière, (3) sur un sujet concernant le salut, la foi ou la morale et (4) avec volonté d'obliger le fidèle à croire. Ceci est bien expliqué dans une [conférence de l'Abbé Lafitte du 21 novembre 2014](https://www.youtube.com/watch?v=l84m2ueOQuQ). Or il me semble tout à fait probable que nombre de déclarations des papes depuis Vatican II qui nous scandalisent ne remplissent pas ces conditions. Entre autre, la volonté d'obliger du pape. Une déclaration informelle ne semble pas revêtir le caractère infaillible. Et les déclarations qui semblent revêtir le caractère infaillible ne semble pas aller en contradiction avec le dogme traditionnel.

En résumé, il reste pour me convaincre de la thèse sédévacantiste à me montrer une déclaration ex-cathedra d'un pape depuis Vatican II qui aurait toutes les caractéristiques mentionnées plus haut et qui entrerait en contradiction avec un dogme passé qui lui aussi doit revêtir un caractère infaillible.

II. La seconde thèse que je rencontre au sujet de l'état de l'église est celle de certains courants non modernistes au sein de l'église post-concile. Les papes depuis le concile sont de vrais papes, et puisque les textes du concile ont été signés, ces textes sont sujets au caractère infaillible si, et seulement si, ils possèdent les caractéristiques de l'infaillibilité. De nouveaux dogmes ont donc pu apparaître. Cependant, les textes du concile étant relativement flous, il est difficile de démêler de qui est pastoral de ce qui est dogmatique. Ce qui est pastoral ne revêt aucun caractère infaillible et peut même être entièrement dans l'erreur, mais nous devons cependant accepter certains nouveaux dogmes. [Arnaud Dumouch, un théologien très actif sur Internet en compte huit](http://docteurangelique.free.fr/cours/10_le_saint_concile_vatican_ii.html) :

-   L’homme est par nature un être libre et la liberté religieuse est une condition de sa nature.
-   L’Ordre des évêques est un ordre indépendant, radicalement non réductible à l’Ordre des prêtres, quoiqu’en dise saint Thomas d’Aquin.
-   Le mariage est ordonné 1° à l’amour réciproque des époux et 2° au don de la vie (et non à la procréation et à l’assouvissement du désir, comme l’enseignait saint Thomas d’Aquin).
-   Les religions autres que le christianisme possèdent en elles des « semences de l’Esprit Saint » qui disposent les âmes des non-chrétiens au salut.
-   Nous devons tenir que Dieu proposera à tous, sans exception, la possibilité d’être sauvé. (C’est le seul dogme à forme solennelle, voir GS 22, 5).
-   Le sacrement de l’eucharistie a pour but l’union par la charité de Dieu et de l’homme (et non seulement la glorification de Dieu).
-   L’infaillibilité pontificale s’exerce de manière extraordinaire, solennelle ou ordinaire (voir définitions du Concile).
-   L’Ecriture sainte n’est pas dictée par Dieu mais inspirée par Dieu à de vrais auteurs humains qui ont écrit avec leurs mots et leur faillibilité. L’Ecriture est infaillible sur la doctrine du salut et sa révélation progressive, pas sur le reste.

Ces nouveaux dogmes peuvent être tout à fait interprétés à la lumière de la tradition. Sur la liberté religieuse, par exemple, le fait que l'homme soit libre de choisir sa religion est un fait de la nature qu'on ne peut pas nier, Chaque homme est libre de choisir de fausses religions, c'est le dogme. Le point de vue pastoral des modernistes visant à mettre toutes les religions, le vrai et le faux, à égalité, reste un point de vue pastoral qu'il n'est pas nécessaire de croire de foi. De même, il me semble tout à fait normal de penser que certaines religions païennes pré-chrétiennes aient pu être inspirées par Dieu, et que certains hommes aient pu se sauver par elles, sans remettre en question la plénitude de la religion catholique qui possède la révélation du Christ que n'ont jamais pu avoir des religions païennes. Mais cela ne veut pas dire qu'il faut cesser d'évangéliser, car les religions païennes n'auront jamais la révélation si on ne leur apporte pas.

Un autre exemple est dans [Gaudium et spes 22,5](http://www.vatican.va/archive/hist_councils/ii_vatican_council/documents/vat-ii_const_19651207_gaudium-et-spes_fr.html#_ftnref38) :

>   En effet, puisque le Christ est mort pour tous et que la vocation dernière de l’homme est réellement unique, à savoir divine, nous devons tenir que l’Esprit Saint offre à tous, d’une façon que Dieu connaît, la possibilité d’être associé au mystère pascal.

Cela semble revêtir tous les caractères de l'infaillibilité : C'est un texte signé par le pape en tant que pape et visant toute l'église. Le sujet concerne la foi et la formulation (nous devons tenir que) semble indiquer un désir d'obliger la foi. De plus, il ne semble pas que ce nouveau dogme soit en contradiction avec des dogmes précédents. Il est possible que certains théologiens passés aient pu avoir des théories contradictoires, mais les toute théorie de théologien ne fait pas automatiquement partie du magistère de l'église.

En conclusion, Il me semble donc tout à fait possible d'accepter les dogmes ayant pu être établis par le concile sans pour autant adhérer à tout le contenu pastoral qui s'y rattache.

III. La troisième thèse est celle de la fraternité Saint Pie X telle que j'ai pu la comprendre. Elle semble dire que le concile Vatican II est entièrement pastoral, parce que c'est ainsi qu'il avait été préparé par Jean XXIII. J'aurais une objection à formuler à ce sujet. Ce qui rend une proposition pastorale ou dogmatique ce n'est pas les intentions du début. D'ailleurs, on voit bien que les textes préparés au début du concile n'ont rien à voir avec ce qui a été adopté à la fin. Ce qui rend une proposition pastorale c'est sa relation particulière à un contexte donné. Par exemple des dispositions qui seraient particulières à un certain pays, ou a une certaine génération, mais non applicable par tout le monde tout le temps. Ce qui est pastoral n'est donc pas infaillible car il ne s'adresse pas a la totalité de l'église.

Or, Gaudium et spes 22,5 _(nous devons tenir que l’Esprit Saint offre à tous, d’une façon que Dieu connaît, la possibilité d’être associé au mystère pascal)_ semble utiliser une formulation dogmatique et non pastorale, et donc au moins cette phrase dans les textes du concile semble indiquer la formulation d'un dogme. Le concile ne serait donc pas entièrement pastoral. Il est donc nécessaire d'étudier tous les textes du concile pour en extraire les formules dogmatiques et les interpréter à la lumière de la tradition.

Une dernière hypothèse qui permettrait de conclure à un concile entièrement pastoral serait une réserve à la signature des textes par le pape Paul VI permettant d’annuler l'effet de toute formule dogmatique dans les textes. Mais ce sont pures suppositions et je n'ai pas davantage d'informations.

En conclusion, mon sentiment personnel est que :

-   Le siège de Rome n'est pas vacant pour les raisons exprimées plus haut.
-   Le concile Vatican II est valide, parce que signé ex-cathedra par le pape en union avec les évêques. De par ce fait, il peut contenir des dogmes de foi.
-   En particulier Gaudium et spes 22,5 exprime un dogme qui ne semble pas contredire la tradition.
-   Que malgré cela, la majorité des textes du concile revêt une formulation pastorale et n'est point soumis à l'infaillibilité.
-   Que les papes depuis Vatican II peuvent également prononcer des dogmes de foi infaillibles, mais qu'ils peuvent également se tromper sur la majorité des opinions pastorales qu'ils expriment
-   Qu'en particulier il est possible que certains papes soient à titre personnel dans l'erreur la plus totale mais que le Bon Dieu nous protège de toute expression ex-cathedra de leur part qui serait erronée.

Au vu de tout cela, je ne vois pas ce qui empêcherait à la Fraternité Saint Pie X de rentrer en pleine communion avec Rome. Comme l'a dit Monseigneur Lefebvre dans la vidéo suivante à 1:07:00 ([La relation par Mgr. Lefebvre du concile no. XXI, dit Vatican II, Montréal, alentour 1982](https://www.youtube.com/watch?v=vC3x3R_nzAw?t=4020)), qu'il ne semble pas y avoir d'oppositions dogmatiques entre lui et Rome.



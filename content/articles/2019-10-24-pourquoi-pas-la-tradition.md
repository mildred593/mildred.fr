---
title: "Pourquoi pas la tradition ?"
date: 2019-10-24
draft: false
---

Cet article fait suite a une analyse que j'ai faite préceddament sous le titre
de ["L'Église et Vatican II"](/articles/2019-02-25-eglise/) dans lequel je
reprenais trois solution à la crise de l'église que nous subissons :

- le sédévacantisme
- la position de la Fraternité Saint Pie X
- l'église conciliaire dans la position de l'herméneutique de la continuité

Je concluais que le sédévacantisme constitue une mauvaise compréhension des
dogmes de l'Église, et que la position de la Fraternité Saint Pie X n'était pas
tenable. Depuis, j'ai lu un livre "La Messe de Toujours" constitué de citations
de Monseigneur Lefebvre et publié en 2005. Ce livre m'a apporté un regard
nouveau sur la position de la Fraternité Saint Pie X que je vais exposer ici.

Nous partons également d'un postulat que le modernisme, s'opposant a la
tradition, ne peut être vrai, et que la nouvelle messe, bien que pouvant être
valide, diminue grandement les bienfaits du sacrement car les vérités n'y sont
plus enseignées.

# Concile Vatican II, pastoral ou dogmatique ?

Tout d'abord, ce qui fait un concile dogmatique, par rapport à un concile
pastoral, c'est qu'il contient des formulations dogmatiques. C'est à dire des
formulations en matière de foi ou de morale qui ont l'intention d'obliger
l'église a croire a ce dogme.

La position de la Fraternité repose sur la vertu de prudence qui consiste a
ignorer les écrits du Concile Vatican II tant que ceux-ci n'ont pas été
explicités par le magistère de l'église. Le concile se voulant pastoral au
départ, les formulation qui s'y trouvent ne sont pas extrêmement claires et si
il contient des dogmes, ceux-ci risquent d'être mal interprétés. Attendons,
donc.

En effet, les formulations qui pourraient correspondre a des dogmes que j'avais
relevées ne sont pas aussi claires que les expressions utilisées par d'autres
conciles. En particulier, Vatican I qui n'est pas si ancien utilise la
formulation suivante:

> [...] nous enseignons et proclamons comme un dogme révélé de Dieu :
> Le pontife romain, lorsqu'il parle ex cathedra, c’est-à-dire lorsque, remplissant sa charge de pasteur et de docteur de tous les chrétiens, il définit, en vertu de sa suprême autorité apostolique, qu'une doctrine, en matière de foi ou de morale, doit être admise par toute l'Église, jouit par l'assistance divine à lui promise en la personne de saint Pierre, de cette infaillibilité dont le divin Rédempteur a voulu que fût pourvue l'Église, lorsqu'elle définit la doctrine sur la foi ou la morale. Par conséquent, ces définitions du Pontife romain sont irréformables de par elles-mêmes et non en vertu du consentement de l'Église.
> Si quelqu'un, ce qu'à Dieu ne plaise, avait la présomption de contredire notre définition qu'il soit anathème.

Il est donc justifié de suspendre notre jugement sur les éventuels dogmes du
concile Vatican II en attendant de meilleures définitions.

Et maintenant, je voulais parler de l'épineux problème de l'autorité qui est
acceptée mais non obéie par la Fraternité.

# L'autorité

Pourquoi donc la Fraternité dit obéir à Rome mais n'applique pas des décisions
du pape ? Pour cela, il est nécessaire de comprendre ce qu'est l'autorité, et ce
qui oblige dans une décision de l'autorité. Cela est expliqué page 379 du livre
précédemment cité.

L'autorité vient toujours de Dieu, et la loi est "une ordonnance de la raison
pour le bien commun" comme disait Léon XIII dans son encyclique _Libertas_. Nous
pouvons rapidement en tirer la conclusion qu'une loi qui imposerait le N.O.M.
(Novus ordro Missae au détriment de la messe de toujours ne doit pas être obéie
car elle instituerait une mauvaise messe au détriment d'une bonne messe, et
n'est donc plus assujettie au bien commun.

<!--
J'en profite pour faire une digression sur la lettre de Saint Paul aux
Éphésiens, chapitre 5, verset 21 à 22, qui dit "Par respect pour le Christ,
soyez soumis les uns aux autres ; les femmes, à leur mari, comme au Seigneur
Jésus ;". La soumission a l'autorité ne tient que parce que l'autorité est
elle-même soumise au bien commun.

Pour revenir à la crise de l'église, il n'est pas juste d'affirmer "préférer se
-->

Il n'est pas non plus juste d'affirmer "préférer se
tromper avec le pape que d'être dans la vérité contre le pape". L'obéissance
suppose une autorité, et l'autorité venant de Dieu uniquement pour permettre
d'atteindre les buts assignés par Dieu ne peut être obéie si elle-même désobéit
a Dieu. Dans ce cas, il est même nécessaire de désobéir a l'autorité qui abuse
de son pouvoir.

C'est en résumé la raison pour laquelle la Fraternité, bien que désobéissant
extérieurement, est en fait réellement soumise à la Rome de toujours.

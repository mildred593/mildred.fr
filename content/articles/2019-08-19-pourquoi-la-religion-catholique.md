---
title: "Pourquoi la religion catholique"
date: 2019-08-19
draft: false
---

Je vais vous partager par ce billet le fruit de ma recherche spirituelle, et la raison qui me pousse au baptême catholique. J'ai choisi la religion catholique alors que j'ai été élevée dans le bouddhisme, et je vais tenter d'expliquer pourquoi.

# I. La recherche de la vérité

Il existe un postulat préalable a la recherche spirituelle, qui peut mener dans deux voies bien distinctes, c'est la position que l'on a par rapport à la vérité. Il existe deux approches : une approche relativiste, et une approche qui considère la vérité comme un absolu. C'est un choix que chacun doit faire en son cœur, décider si on considère qu'il existe une vérité absolue, immuable et parfaite, ou si au contraire « à chacun sa vérité ».

Le postulat relativiste est élégant. Nous avons été élevés ainsi, et la pensée trouve plus facilement une approche dans l'idée que tout dépend du contexte. La physique moderne est relativiste, et l'opinion dominante semble relativiste également. Nous vivons de fait dans un monde baigné de relativisme.

Alors, pourquoi s'éloigner du relativisme et chercher un absolu ?

Une première réponse serait de dire que la recherche spirituelle consiste justement a chercher l'absolu immuable, à l'origine du monde, et de qui tout est issu.

Une seconde réponse pourrait mentionner que l'approche relativiste ne pourra mener à rien de certain. Si à chacun sa vérité, alors quelle est-elle ? Nous ne pouvons être sûr.

Ce qui m'a permis de mettre le doigt sur ce problème de l'approche relativiste fut des [conférences de Mark Passio sur son site](http://www.whatonearthishappening.com/). Cette personne reste pour moi une des personnes qui fondent ma recherche spirituelle une fois que je me suis détachée du bouddhisme. Mark Passio est une personne qui se décrit ancien sataniste, et décrit le complot mondial de Satan contre Dieu. Complot sataniste que j'avais déjà découvert au préalable.

Les [premiers épisodes de son podcast](hhttp://www.whatonearthishappening.com/podcast-in-order/) dont le premier a pour titre *Truth, Deception, Solipsism (The Biggest Lie), Consciousness, the Human Brain* nous donnent une information vitale. Il y explique le solipsisme qui est une vision du monde dans lequel il n'y aurait pour le sujet pensant d'autre réalité acquise avec certitude que lui-même. C'est la vision du monde que j'avais jusqu'alors, qui est basée sur la logique la plus pure et sur un doute total des perceptions extérieures. Il explique, mieux que moi, comment cette vision du monde est une impasse spirituelle, car elle bloque toute tentative de rechercher l'absolu. C'est une vision du monde mise en avant par les sectes satanistes, et nous pouvons les voir à l'œuvre facilement.

L'alternative est de croire qu'il existe un univers qui n'est pas absurde. et c'est le premier acte de foi volontaire à poser. Il n'y a pas moyen d'utiliser des arguments logiques pour arriver a ce postulat, et il faut donc choisir d'y croire parce que cela fait sens. Une fois cela posé, nous sommes alors armés pour découvrir le sens de l'univers qui nous entoure.

Sans cette base commune, il est impossible de discuter davantage.

# II. La connaissance

## L'hermétisme

La première pensée récente a venir me troubler dans ma recherche de la vérité furent des écrits sur l'hermétisme. En particulier un petit texte qu'on appelle *Le Kybalion : Étude sur la philosophie hermétique de l'ancienne Égypte et de l'ancienne Grèce*. Il explique le monde, et son fonctionnement, de manière très proches de ce que j'avais pu comprendre par ailleurs. J'ai rapidement fait un rapprochement avec les [théories physiques de Gabriel LaFrenière](http://mildred.github.io/glafreniere/) dont je suis fan depuis longtemps.

Ce texte rapporte sept principes généraux qui gouvernent le la philosophie hermétique :

* le principe de mentalisme : le Tout est esprit ; l'univers est mental.

* le principe de correspondance : ce qui est en haut est comme ce qui est en bas ; ce qui est en bas est comme ce qui est en haut. L'univers est fractal et le microcosme est semblable au macrocosme. L'un peut être utilisé pour connaître l'autre

* le principe de vibration : le mouvement se manifeste partout dans l'univers, que rien n'est à l'état de repos, que tout se remue, vibre

* le principe de polarité : tout a deux pôles, et les deux pôles sont identiques en nature mais différents en degrés

* le principe de rythme : en toute chose se manifeste un mouvement d'allée et venue entre deux pôles

* le principe de cause et d'effet : tout arrive conformément a la loi et rien n'arrive fortuitement. Le hasard (ou la chance) n'est que le nom donné à la loi méconnue.

* le principe de genre : différent du sexe, l'équilibre du masculin et du féminin permet une action réussie. Le masculin a pour rôle de diriger la volonté sur le féminin qui va créer en fonction de l'impulsion donnée.

C'est une bonne entrée en matière, mais faute d'enseignement supplémentaire pratique, il est difficile d'en tirer une morale ou une ligne de conduite. Cela m'a donné envie de fouiller davantage et me laissait un goût de trop peu.

## La gnose

L'univers a conspiré pour que je cherche ensuite du coté de la gnose. L'hermétisme ayant fondé quelques bases, je suis ensuite passée par les livres d'Anton Parks et également par le groupe de musique identitaire ["Les Brigandes"](https://lesbrigandes.com/), [son clan](https://le-clan-des-brigandes.fr/) et plus particulièrement, son idéologie et les écrits de Joël LaBruyère. Cela me parle tout particulièrement, car il fut a l'époque l'auteur de la revue Undercover, revue anti-conspirationniste fort intéressante, et auteur d'un texte ["les fumées du nouvel âge"](http://www.ateliersante.ch/fumee.htm) que j'avais lu dés les années 2000.

J'ai donc lu avec attention "Kali Yuga, Lumières sur la civilisation de l'Âge Noir" de Joël LaBruyère, que j'ai téléchargé, imprimé et relié, et je commence a me familiariser avec un vocabulaire gnostique chrétien. Cela permet de faire une traduction entre le monde chrétien et son vocabulaire, tout en restant dans une idéologie proche de ce que j'ai pu connaître avec le bouddhisme et la non dualité. Ce fut très progressif et c'est difficile a raconter ici.

Le déclic se passe avec la lecture des livres d'Anton Parks, pratiquement tous, où on découvre une histoire romancée des personnages de la gnose. Arrivant au terme de ses livres, j'ai donc voulu en savoir davantage sur ces personnages attachants, et j'ai commencé a lire l'évangile apocryphe Pistis Sophia. Cet évangile raconte l'histoire de Jésus Christ qui enseigne a ses apôtres, bien après l'ascension de Notre Seigneur au ciel, les concepts gnostiques.

Le texte est assez obscur en lui même et j'ai donc lu des commentaires du texte. Le premier est la traduction et le commentaire de Emile Amelineau, et ensuite le commentaire de Jan van Rijckenborgh (rose-croix d'or).

Pistis Sophia dérit le chemin d'une âme au travers des 12 éons de ce monde, qui représentent toutes les déceptions possibles sur le chemin spirituel, pour arriver vers Dieu. Le treizième éon joue un rôle particulier car même si il reste de ce monde et n'est pas de Dieu, il a pour rôle de diriger les âmes vers Dieu. 13 représente ici le Christ.

Le monde gnostique est un monde difficile où la majorité des âmes sont condamnées a vivre loin de Dieu, emprisonnées dans ce monde et les 12 éons qui cherchent a créer un écosystème séparé de Dieu. Le chemin vers Dieu est semé d'embûches et est extrêmement ardu. La connaissance, la gnose, est primordiale pour discerner le bon chemin du mauvais.

Sur le chemin de la gnose, il faut faire encore plus attention, car la connaissance pose elle aussi un obstacle pour rejoindre Dieu. Elle peut avoir deux usages : le premier mondain, où la connaissance permet de tirer parti du monde qui nous entoure pour gagner davantage de richesse ou de pouvoir, et le second spirituel, où la connaissance permet de se libérer des chaînes du monde et rejoindre Dieu.

Les treize étapes du chemin gnostiques peuvent être résumées dans ces treize chants :

1. Dans le premier chant, la Pistis Sophia découvre la nature dialectique et la
   condamnation de l'humanité. Elle entonne le Chant de l'Humanité.

2. Dans le deuxième chant elle en vient à la découverte de son état naturel. C'est le Chant de la Conscience.

3. A partir de là elle fait entendre le Chant de l'Humilité vis-à-vis de l'Unique et Véritable Lumière.

4. Puis vient le Chant du Brisement : le moi est porté en terre.

5. Le Chant de la Résignation fait suite : la Pistis Sophia fait le don total d'elle-même.

6. Sur cette base retentit le Chant de la Confiance, où la Lumière est implorée avec une foi et une confiance totales.

7. Dans sa septième repentance, la Pistis Sophia chante le Chant de la Décision. C'est la montée ou la chute.

8. Ensuite a lieu la persécution. Les éons de la nature s'attaquent avec force à la Pistis Sophia, qui entonne le Chant de la Persécution.

9. Après le Chant de la Percée, elle se débarrasse positivement de son ennemi.  

10. Ensuite elle chante le Chant de l'Exaucement. La Pistis Sophia voit pour la première fois la Lumière des Lumières.

11. La force de la foi intérieure est soumise à l'épreuve finale. La Pistis Sophia fait retentir le Chant de l'Epreuve de la foi.

12. Elle subit alors la grande épreuve que l'on peut comparer à la tentation dans le désert. Elle chante le Chant de la Grande Epreuve.

13. Enfin sa treizième repentance est le Chant de la Victoire: l'âme s'est élevée, elle voit et rencontre l'Esprit, son Py-mandre.

Cet évangile reprend également nombre de psaumes dans le contexte gnostique.

## La révélation

Ce cheminement particulier m'a permis d'acquérir le vocabulaire chrétien. Je me suis donc naturellement interrogée sur la conception catholique du monde que je ne connaissait pas, mais que le vocabulaire chrétien m'a permis d'aborder plus facilement. Ce sont les [cours en vidéo de catéchisme de l'Abbé Laguérie](https://blog.institutdubonpasteur.org/-Catechisme-en-video-) qui ont obtenu ma faveur.

J'ai pensé un moment suivre une formation à la fois catholique et gnostique, rose croix d'or, afin de comparer, mais les tendances pro-nouvel-ordre-mondial du milieu gnostique m'en a dissuadé.

Les premières vidéos sont très intéressantes parce qu'elles ne supposent pas un pré-requis de foi. Elles s'adressent à tous et montrent l'existence de Dieu de manière purement philosophique, sans nécessiter d'accepter la révélation.

Ces vidéos furent pour moi une grande surprise car j'y ai compris bon nombre de choses. En particulier, j'avais beaucoup de mal avec Dieu présenté comme une personne (et sans parler de la trinité). Dieu c'était pour moi le Tout de l'hermétisme, ou l'absolu gnostique. Pas quelqu'un d'individualisé. Ce que ce catéchisme m'a enseigné c'est que Dieu était avant tout la cause primaire du monde comme je l'avais toujours pensé.

La foi catholique peut se résumer par le symbole des apôtres :

> Je crois en un seul Dieu, le Père tout-puissant, Créateur du ciel et de la terre,

Dieu se définit par l'être qui est cause de toute chose. Cette définition est amplement suffisante et permet de surmonter les problèmes de conception d'un Dieu trop personnifié. Dieu étant cause de tout, il est donc tout-puissant. On le dit créateur également parce qu'étant cause de tout, on considère qu'il a créé toute chose.

La religion catholique n'est pas panthéiste, Dieu n'est pas «le tout». Dieu est la cause de tout mais il a créé l'univers à partir de rien. Il n'est pas identifié a l'univers. C'est différent de ce que je croyais à la base, mais aucun argument peut faire pencher en faveur du panthéisme, donc pourquoi pas. Et finalement, cette différence avec l'hermétisme n'est pas fondamentale pour le salut.

> et en Jésus-Christ, son Fils unique, notre Seigneur,

Nous entrons ici dans le vif du sujet, Dieu a un fils qui s'est incarné. Nous commençons ici a toucher a la trinité. D'un point de vue extérieur, la trinité n'est pas vitale a comprendre car elle touche a l'intimité de Dieu. Lorsqu'on attribue des actions au Père, au Fils ou au Saint-Esprit, ce ne sont que des attributions que l'on donne, car en réalité, l'entièreté de Dieu participe a toutes les actions de chacune des personnes.

D'un point de vue païen (au bon sens du terme, la religion naturelle sans la révélation du Christ) il suffit de considérer chacune des personnes de la trinité comme Dieu tout entier car ce n'est pas différentiable de l'extérieur.

L'incarnation, Dieu qui se fait homme, a la fois vrai homme et vrai Dieu, est importante pour la révélation. Jésus Christ nous donne des informations, des dogmes de foi, qu'il est impossible de connaître par la philosophie. Il nous montre le chemin vers lui.

Ce qui est le plus choquant dans l'incarnation c'est de penser que Dieu, éternel et infini, choisisse de se révéler à nous par un homme inscrit dans le temps et dans l'espace, il y a environ 2000 ans en Judée. Cela heurte notre recherche d'idéal divin. Dieu ne peut se révéler que de manière éternelle et omniprésente, serions nous tentés de penser.

Cette révélation éternelle et omniprésente est celle des païens, la religion naturelle tournée vers Dieu par la recherche philosophique.

Mais ce qu'il faut savoir c'est que Dieu a un plan de salut des âmes le plus parfait possible, Dieu étant lui même parfait. Il n'est pas possible de sauver davantage d'âmes que celles qui seront sauvées de toute éternité. Ce plan de libération, il est logique de penser qu'il l'a adapté a l'homme afin que l'homme puisse plus facilement comprendre Dieu. Ainsi, une incarnation devient logique.

> a souffert sous Ponce Pilate, a été crucifié, est mort, a été enseveli, est descendu aux enfers ;

Nous apprenons ici la passion de Notre Seigneur. Il y a le fait historique d'une part, qui ancre la vie de Jésus dans la réalité, et il y a le principe de la mort de Jésus, qui se sacrifie de lui même, afin de nous permettre le salut.

En effet, le péché originel, la première offense contre Dieu, a blessé complètement l'humanité. Pourvus de dons particuliers lors de la création d'Adam et Ève, nous les avons perdus et sommes dés lors soumis aux tentations et à la faute. Ce péché originel est infiniment grave car il a été fait contre Dieu, qui est nous est infiniment supérieur.

Le principe de justice, qu'on peut appeler aussi le karma, nous oblige a une réparation de nos péchés en proportion. Et Dieu, dans son infinie miséricorde trouve un moyen de satisfaire à nos péchés sans contrevenir a son infinie justice. Il s'offre lui-même en sacrifice, un sacrifice d'une valeur infinie car il est Dieu, en réparation de tous nos péchés.

C'est donc la raison pour laquelle il est mort à la suite du supplice de la croix. La tradition nous enseigne qu'il est descendu aux enfers. Il ne s'agit pas là d'une descence dans l'enfer des damnés, mais aux enfers. Ce sont les bardos, un lieu de transition où les âmes qui sont mortes avant la résurrection attendent leur jugement.

> le troisième jour, est ressuscité des morts ;

Il se ressuscite seul, sans aide extérieure, alors qu'il est mort. C'est un vrai miracle et prouve que Jésus-Christ est vrai Dieu.

> est monté au ciel, est assis à la droite de Dieu, le Père tout-puissant ;
> 
> d'où Il viendra juger les vivants et les morts.

Le jugement des âmes correspond à la justice de Dieu qui est de donner a chacun selon ses mérites. C'est le karma, qui implique de satisfaire a chacun de nos péchés. Le moment de la mort est primordial car soit, par nos péchés, nous sommes en révolte contre Dieu et nous refusons son sacrifice. En conséquence de quoi nous nous coupons de Dieu pour l'éternité. Soit, selon notre amour de Dieu (la charité), nous accédons directement au Paradis, la vision béatifique de Dieu, ou nous restons pour un temps au purgatoire afin de nous purifier avant de pouvoir voir Dieu face à face.

Le terme de jugement est adapté, car c'est un moment décisif et unique. Une fois morts, nous perdons la capacité de modifier le cours des évènements, et nous entrons dans l'éternité. L'instant de la mort nous fige là où nous en sommes.

C'est également la raison pour laquelle les mauvais anges sont condamnés sans espérance de salut car étant purs esprits, ils vivent dans l'éternité. leur révolte est définitive sans possibilité de salut.

> Je crois en l'Esprit-Saint

L'esprit Saint peut se comparer au maître intérieur, qui nous guide au cours de notre vie pour trouver le chemin de Dieu.

> à la sainte Église catholique, la communion des saints,

L'incarnation nous oblige a considérer que Jésus-Christ, vrai Dieu, nous a légué un héritage à travers l'église catholique. L'incarnation est la raison pour laquelle l'église catholique est universelle et pourquoi les autres églises chrétiennes non apostoliques n'ont pas de sens. Cet héritage justifie la foi chrétienne, et se couper de cet héritage revient a une religion païenne. Sans lui, il n'y a pas de foi et on accède à Dieu par la religion naturelle.

L'église, c'est aussi les saints au ciel, qui peuvent intercéder pour nous. Les saints étant plus proches de Dieu savent mieux que nous quoi prier et comment prier pour nous exaucer au mieux. Dieu préfère les prières des saints car elles sont mieux ordonnées a notre fin, sont plus morales, que nos propres prières maladroites.

> la rémission des péchés,

Nous croyons également que par le sacrifice de Notre Seigneur Jésus Christ, actualisé à chaque messe, nous sommes capable de restauration même après avoir commis le péché. C'est un privilège immense que nous devons accueillir car il signifie que Dieu est infiniment miséricordieux, et que nous avons une vraie chance de salut. 

> la résurrection de la chair, la vie éternelle

La résurrection de la chair, c'est après notre mort, une fois le jugement passé, Dieu nous donnera un nouveau corps. Nous sommes humains, donc composés d'un esprit et d'un corps, qui sera restauré pour nous.

C'est difficile a imaginer un corps physique tel que le nôtre sur cette terre. Et de fait, il ne sera pas identique. L'imaginer tel un corps fait de matière subtile peut aider a imaginer ce que cela pourrait être

Point délicat, mais le corps qui nous sera donné ne sera pas un corps identique au nôtre d'aujourd'hui mais un corps ... qui n'est peut être pas autant ancré dans la matière qu'a présent.

L'esprit est éternel, la vie ne peut être qu'éternelle, que nous allions auprès de Dieu ou que nous choisissons par nos péchés de nous en écarter pour toujours.

# La grâce

J'aimerais terminer cet article sur un aspect très important de la religion
catholique, la grâce, qui est le chemin vers la libération ou le salut. La grâce
est un don surnaturel qui nous vient de Dieu lorsqu'on pratique la foi,
l'espérance, et la charité (qu'on nomme les trois vertus théologales). La grâce
s'obtient le mieux au sein de la religion catholique, mais on peut la trouver
avant la venue de Notre Seigneur Jésus Christ, ou chez les païens.

La foi, c'est l'adhésion de l'intelligence aux vérités de Dieu. C'est un acte de
volonté et ce n'est pas un sentiment comme on l'entend souvent. Les rudiments de
la foi, c'est la croyance en une vérité absolue, et également la croyance en un
seul Dieu (auquel on peut arriver par la raison, Dieu étant la cause de toute
chose, cette cause devant nécessairement être unique, sinon il y aurait un autre
Dieu qui les aurait causés).

L'espérance, c'est la volonté de s'approcher et de mieux connaître Dieu, soutenu
par la croyance qu'il est rémunérateur pour ceux qui le cherchent. C'est ce qui
nous amène à le prier et a dépendre de lui pour notre secours. Sans l'espérance,
point de Salut car Dieu ne nous sauve pas contre notre propre volonté. Nous
devons vouloir être sauvés.

Enfin, la charité est un amour de Dieu pour lui même, et l'amour de sa création
pour Dieu. C'est une relation intime avec Dieu, une attirance qui nous portera
irrémédiablement à lui. Comment peut on imaginer être rémunéré par la vision
béatifique de Dieu alors qu'on ne l'aime pas ? La charité est nécessaire. C'est
également la charité qui nous pousse aux bonnes œuvres, car en aimant Dieu, nous
aimons également notre prochain pour Dieu. La charité n'est pas l'amour du
prochain en abstraction de Dieu car c'est parce que Dieu aime notre prochain que
nous l'aimons aussi de charité. Également, comment dire que nous vivons de
charité si nous commettons des actes mauvais contre nous, Dieu ou notre
prochain ? Tout acte mauvais nous éloigne de l'amour de Dieu, comme tout acte
contre notre conjoint dans un couple nous éloigne de son amour.

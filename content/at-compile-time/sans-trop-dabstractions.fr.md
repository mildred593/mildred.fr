---
title: Sans trop d'abstractions
date: 2020-12-30T22:01:41.833Z
draft: true
---
Go est le bon exemple à forcer à écrire ses boucles for. Des abstractions trop évoluées mènent à du code non performant. Les fonctions de base devraient avoir un coût similaire et faible. Un programmeur va se poser des questions si il écrit beaucoup de code mais pas si il appelle une seule fonction en une seule ligne.
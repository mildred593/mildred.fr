---
title: Optimisé
date: 2020-12-30T22:04:29.112Z
draft: true
---
Car c'est important, less is more. Faire attention aux appels systèmes et surtout à tout ce qui est IO. Faire attention à sa consommation mémoire. Ne pas trop faire dépendre son code de services pouvant ralentir le processus mais permettre l'injection de dépendances.
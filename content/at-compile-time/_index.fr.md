---
title: Avant la compilation
---

Quel est cet instant fugitif, juste avant la compilation ou tout est encore
possible mais tout est déjà écrit ?

